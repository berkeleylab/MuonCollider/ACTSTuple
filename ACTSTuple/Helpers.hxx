#ifndef Helpers_h
#define Helpers_h 1

#include <lcio.h>

/** Helpers for working with LCIO
 * 
 * @author Karol Krizka
 * @version $Id$
 */
namespace Helpers
{
  /** helper function to get collection safely */
  lcio::LCCollection* getCollection(lcio::LCEvent* evt, const std::string& name );
};

#endif
