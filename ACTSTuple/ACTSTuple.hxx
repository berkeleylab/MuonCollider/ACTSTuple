#ifndef ACTSTuple_h
#define ACTSTuple_h 1

#include <marlin/Processor.h>

#include "CSVOutput.hxx"

//! \brief Creates a simple, ACTS-compatible, CSV files from LCIO collections.
/**
 *  <h4>Input - Prerequisites</h4>
 *  Needs collections of TrackerHits
 *
 *  <h4>Output</h4> 
 *  A set of CSV files with event data.
 * 
 * @author Karol Krizka
 * @version $Id$
 */
class ACTSTuple : public marlin::Processor
{
  /*  
  struct PIDBranchDef{
    std::string algoName{} ;
    std::string prefix{} ;
    std::vector<std::string> pNames{} ;
    std::vector<std::string> bNames{} ;
  } ;
  */

public:
  
  virtual marlin::Processor* newProcessor() { return new ACTSTuple ; }
  
  ACTSTuple(const ACTSTuple &) = delete ;
  ACTSTuple& operator =(const ACTSTuple &) = delete ;
  ACTSTuple() ;

  /** Called at the begin of the job before anything is read.
   * Use to initialize the processor, e.g. book histograms.
   */
  virtual void init() ;

  /** Called for every run.
   */
  virtual void processRunHeader( LCRunHeader* run ) ;

  /** Called for every event - the working horse.
   */
  virtual void processEvent( LCEvent * evt ) ; 
  
  virtual void check( LCEvent * evt ) ; 

  /** Called after data processing for clean up.
   */
  virtual void end() ;
  
protected:
//   /*
//   void decodePIDBranchDefinitions() ;

  /** Input collection name.
   */
  std::string _mcpColName {};
//   std::string _mcpRemoveOverlayColName {};
//   std::string _recColName {};
//   std::string _jetColName {};
//   std::string _isolepColName {};  
//   std::string _trkColName {};
//   std::string _cluColName {};
  std::string _sthColName {};
  std::string _trhColName {};
//   std::string _schColName {};
//   std::string _cahColName {};
//   std::string _vtxColName {};
//   std::string _pfoRelName {};
//   std::string _relName {};

//   bool _jetColExtraParameters {};                 /* Enables writing extra jet parameters */
//   bool _jetColTaggingParameters {};               /* Enables writing jet tagging parameters */


//   StringVec _relColNames {};
//   StringVec _relPrefixes {};

//   StringVec _pidBranchDefinition {};
  
  std::string _outputDir {};

//   CWOutputSet* _evtOutput {};
  std::shared_ptr<CSVOutput> _mcpOutput;
//   CollectionOutput* _mcpremoveoverlayOutput {};
//   CollectionOutput* _recOutput {};
// //  CollectionOutput* _jetOutput {};
//   JetOutput* _jetOutput {};
//   CollectionOutput* _isolepOutput {};
//   CollectionOutput* _trkOutput {};
//   CollectionOutput* _cluOutput {};
  std::shared_ptr<CSVOutput> _sthOutput;
  std::shared_ptr<CSVOutput> _trhOutput;
//   CollectionOutput* _schOutput {};
//   CollectionOutput* _cahOutput {};
//   CollectionOutput* _vtxOutput {};
//   MCParticleFromRelationOutput* _mcRelOutput {};
  
//   std::vector<PIDOutput*> _pidOutputVec {};
//   std::vector<CWOutputSet*> _relOutputVec {};
  
//   int _nRun {};
//   int _nEvt {};

//   std::vector<PIDBranchDef> _pidBranchDefs {} ;
//   */
};

#endif
