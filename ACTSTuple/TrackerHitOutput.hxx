#ifndef TRACKERHITOUTPUT_HXX
#define TRACKERHITOUTPUT_HXX 1

#include "CSVOutput.hxx"

/** TrackerHitOutput holds branches created from LCRelations.
 * 
 * @author Karol Krizka
 * @version $Id$
 */

class TrackerHitOutput : public CSVOutput
{  
public:
  TrackerHitOutput(const std::string& outputDir, const std::string& table);
  virtual ~TrackerHitOutput() =default;

  virtual void open(EVENT::LCEvent* evt);
  virtual void fill(const EVENT::LCCollection* col, EVENT::LCEvent* evt ) ;
};

#endif
