#ifndef CSVOUTPUT_HXX
#define CSVOUTPUT_HXX 1

#include <EVENT/LCCollection.h>
#include <EVENT/LCEvent.h>

#include <string>
#include <fstream>

//! Base class for dumping data in CSV files
/** 
 * The base class is responsible for opening the correctly
 * named file for a given collection and event number.
 *
 * @author Karol Krizka
 */

class CSVOutput
{
public:
  CSVOutput(const std::string& outputDir, const std::string& table);
  virtual ~CSVOutput() =default;

  virtual void open(EVENT::LCEvent* evt);
  
  virtual void fill(const EVENT::LCCollection* col, EVENT::LCEvent* evt ) =0;

protected:
  std::ofstream _fh;
  
  std::string _outputDir;
  std::string _table;
};

#endif
