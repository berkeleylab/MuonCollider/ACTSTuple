#ifndef MCPARTICLEOUTPUT_HXX
#define MCPARTICLEOUTPUT_HXX 1

#include "CSVOutput.hxx"

/** MCParticleOutput holds branches created from LCRelations.
 * 
 * @author Karol Krizka
 * @version $Id$
 */

class MCParticleOutput : public CSVOutput
{  
public:
  MCParticleOutput(const std::string& outputDir, const std::string& table);
  virtual ~MCParticleOutput() =default;

  virtual void open(EVENT::LCEvent* evt);
  virtual void fill(const EVENT::LCCollection* col, EVENT::LCEvent* evt ) ;
};

#endif
