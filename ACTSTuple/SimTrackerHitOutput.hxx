#ifndef SIMTRACKERHITOUTPUT_HXX
#define SIMTRACKERHITOUTPUT_HXX 1

#include "CSVOutput.hxx"

/** SimTrackerHitOutput holds branches created from LCRelations.
 * 
 * @author Karol Krizka
 * @version $Id$
 */

class SimTrackerHitOutput : public CSVOutput
{  
public:
  SimTrackerHitOutput(const std::string& outputDir, const std::string& table);
  virtual ~SimTrackerHitOutput() =default;

  virtual void open(EVENT::LCEvent* evt);
  virtual void fill(const EVENT::LCCollection* col, EVENT::LCEvent* evt ) ;
};

#endif
