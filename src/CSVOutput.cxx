#include "CSVOutput.hxx"

#include <Exceptions.h>

#include <lcio.h>
#include <marlin/VerbosityLevels.h>

#include <iomanip>

using namespace lcio ;
using namespace marlin ;

CSVOutput::CSVOutput(const std::string& outputDir, const std::string& table)
  : _outputDir(outputDir), _table(table)
{ }

void CSVOutput::open(EVENT::LCEvent* evt)
{
  // Close previous file
  if(_fh.is_open())
    _fh.close();

  // Open new one!
  std::stringstream ss;
  ss << _outputDir << "/";
  ss << "event" << std::setw(9) << std::setfill('0') << evt->getEventNumber();
  ss << "-" << _table << ".csv";
  
  _fh.open(ss.str());
}






















