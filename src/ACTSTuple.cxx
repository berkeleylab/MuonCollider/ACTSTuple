#include "ACTSTuple.hxx"

#include "Helpers.hxx"

#include "MCParticleOutput.hxx"
#include "SimTrackerHitOutput.hxx"
#include "TrackerHitOutput.hxx"

#include <cstring>

#include <sys/stat.h>


ACTSTuple aACTSTuple ;


ACTSTuple::ACTSTuple()
  : Processor("ACTSTuple")
{
  // modify processor description
  _description = "Creates a simple, ACTS-compatible, CSV file from LCIO collections." ;

  registerProcessorParameter( "ACTSOutputDir" ,
                              "Path to output directory",
			      _outputDir,
			      std::string("output/")
			      );

  // register steering parameters: name, description, class-variable, default value
  registerInputCollection( LCIO::MCPARTICLE,
			   "MCParticleCollection", 
			   "Name of the MCParticle collection",
			   _mcpColName,
			   std::string("")
			   );

  registerInputCollection( LCIO::SIMTRACKERHIT,
			   "SimTrackerHitCollection", 
			   "Name of the SimTrackerHit collection",
			   _sthColName ,
			   std::string("")
			   );  

  registerInputCollection( LCIO::TRACKERHIT,
			   "TrackerHitCollection", 
			   "Name of the TrackerHit collection",
			   _trhColName ,
			   std::string("")
			   );  
}

void ACTSTuple::init()
{
  streamlog_out(DEBUG) << "   init called  - create directory with name : "  <<  _outputDir << " with columnwise ntuple with LCIO data" << std::endl;

  // Try to create output directory, if needed
  struct stat sb;
  if(stat(_outputDir.c_str(), &sb) != 0)
    { // Does not exist
      if(mkdir(_outputDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
	throw std::runtime_error("Unable to create directory "+_outputDir+": "+std::strerror(errno));
    }

  //=====================================================
  //    initialize the branches 

  //_evtOutput = std::make_shared<EventOutput>();
  //_evtOutput->initOutput( _tree ) 

  if( _mcpColName.size() )
    {
      _mcpOutput = std::make_shared<MCParticleOutput>(_outputDir,"particles");
    }

  if( _sthColName.size() )
    {
      _sthOutput = std::make_shared<SimTrackerHitOutput>(_outputDir,"realsimhits");
    }

  if( _trhColName.size() )
    {
      _trhOutput = std::make_shared<TrackerHitOutput>(_outputDir,"simhits");
    }
}

void ACTSTuple::processRunHeader( LCRunHeader* run )
{ }

void ACTSTuple::processEvent( LCEvent * evt )
{
  if( _mcpOutput )
    {
      _mcpOutput->open(evt);

      LCCollection *mcpCol =  Helpers::getCollection ( evt , _mcpColName ) ;
      if( mcpCol ) _mcpOutput->fill( mcpCol , evt );
    }

  if( _sthOutput )
    {
      _sthOutput->open(evt);
  
      LCCollection *sthCol =  Helpers::getCollection ( evt , _sthColName ) ;
      if( sthCol ) _sthOutput->fill( sthCol , evt );
    }

  if( _trhOutput )
    {
      _trhOutput->open(evt);
  
      LCCollection *trhCol =  Helpers::getCollection ( evt , _trhColName ) ;
      if( trhCol ) _trhOutput->fill( trhCol , evt );
    }

  //================================================

  streamlog_out(DEBUG) << "   processing event: " << evt->getEventNumber() 
		       << "   in run:  "          << evt->getRunNumber()   << std::endl ;
}
  
void ACTSTuple::check( LCEvent * evt )
{ }

void ACTSTuple::end()
{ }
