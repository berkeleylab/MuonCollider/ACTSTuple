#include "MCParticleOutput.hxx"

#include <lcio.h>
#include <marlin/VerbosityLevels.h>

#include <EVENT/MCParticle.h>
#include <EVENT/TrackerHitPlane.h>

#include <UTIL/CellIDDecoder.h>

MCParticleOutput::MCParticleOutput(const std::string& outputDir, const std::string& table)
  : CSVOutput(outputDir, table)
{ }

void MCParticleOutput::open(EVENT::LCEvent* evt)
{
  CSVOutput::open(evt);

  _fh << "particle_id,particle_type,process,vx,vy,vz,vt,px,py,pz,m,q" << std::endl;
}

void MCParticleOutput::fill(const EVENT::LCCollection* col, EVENT::LCEvent* evt )
{
  streamlog_out( DEBUG2 ) << " MCParticleOutput::fill called" << std::endl;

  if( col->getTypeName() != lcio::LCIO::MCPARTICLE )
    {
      std::string exStr("MCParticleOutput::fill: invalid collection type: " + col->getTypeName() ) ;
      throw EVENT::Exception( exStr ) ; 
    }

  for(uint32_t i=0; i < col->getNumberOfElements(); ++i)
    {
      lcio::MCParticle* mcp = static_cast<lcio::MCParticle*>( col->getElementAt(i) ) ;

      // particle_id
      uint64_t barcode=0;

      //_fh << mcp->id() << ","; // Probably not the best thing, but not sure it matters
      _fh << 4503599644147712 << ","; // Hardcode a scatter thingy
      // particle_type
      _fh << mcp->getPDG() << ",";

      // process
      _fh << 0 << ",";

      // vx,vy,vz,vt
      _fh << mcp->getVertex()[0] << "," << mcp->getVertex()[1] << "," << mcp->getVertex()[2] << "," << mcp->getTime() << ",";

      // px,py,pz,m
      _fh << mcp->getMomentum()[0] << "," << mcp->getMomentum()[1] << "," << mcp->getMomentum()[2] << "," << mcp->getMass() << ",";

      // q 
      _fh << mcp->getCharge();

      // Done..
      _fh << std::endl;
    
    }
}






















