#include "Helpers.hxx"

#include <marlin/VerbosityLevels.h>

namespace Helpers
{
  lcio::LCCollection* getCollection(lcio::LCEvent* evt, const std::string& name )
  {   
    if( name.size() == 0 )
      return nullptr;

    try
      {  
        return evt->getCollection( name ) ;
      }
    catch(const lcio::DataNotAvailableException& e )
      {  
        streamlog_out( DEBUG2 ) << "getCollection :  DataNotAvailableException : " << name <<  std::endl ;

        return nullptr;
      }
  }
}
  
