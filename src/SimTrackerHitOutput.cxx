#include "SimTrackerHitOutput.hxx"

#include "ACTSGeometryIdMappingTool.hxx"

#include <lcio.h>
#include <marlin/VerbosityLevels.h>

#include <EVENT/MCParticle.h>
#include <EVENT/SimTrackerHit.h>
#include <EVENT/TrackerHitPlane.h>

#include <UTIL/CellIDDecoder.h>

#include <iomanip>

SimTrackerHitOutput::SimTrackerHitOutput(const std::string& outputDir, const std::string& table)
  : CSVOutput(outputDir, table)
{ }

void SimTrackerHitOutput::open(EVENT::LCEvent* evt)
{
  CSVOutput::open(evt);

  _fh << "particle_id,geometry_id,system_id,layer_id,side_id,module_id,sensor_id,tx,ty,tz,tt,tpx,tpy,tpz,te,deltapx,deltapy,deltapz,deltae,index" << std::endl;
  _fh << std::setprecision(8);
}

void SimTrackerHitOutput::fill(const EVENT::LCCollection* col, EVENT::LCEvent* evt )
{
  streamlog_out( DEBUG2 ) << " SimTrackerHitOutput::fill called" << std::endl;

  if( col->getTypeName() != lcio::LCIO::SIMTRACKERHIT )
    {
      std::string exStr("SimTrackerHitOutput::fill: invalid collection type: " + col->getTypeName() ) ;
      throw EVENT::Exception( exStr ) ; 
    }

  std::string encoderString = col->getParameters().getStringVal( "CellIDEncoding" );

  UTIL::CellIDDecoder<lcio::SimTrackerHit> decoder(encoderString);
  ACTSGeometryIdMappingTool geoIDtool(encoderString);

  for(uint32_t i=0; i < col->getNumberOfElements(); ++i)
    {
      lcio::SimTrackerHit* hit = static_cast<lcio::SimTrackerHit*>( col->getElementAt(i) ) ;

      uint64_t geoID    = geoIDtool.getGeometryID(hit);

      uint32_t systemID = decoder(hit)["system"];
      uint32_t layerID  = decoder(hit)["layer" ];
      uint32_t sideID   = decoder(hit)["side"  ];
      uint32_t ladderID = decoder(hit)["module"];
      uint32_t moduleID = decoder(hit)["sensor"];

      // particle_id
      //_fh << hit->getMCParticle()->id() << ",";
      _fh << 4503599644147712 << ","; // Hardcode a scatter thingy

      // geometry_id
      _fh << geoID << ","; // Not sure if this is right...
      
      // system_id
      _fh << systemID << ","; // Not sure if this is right...

      // layer_id
      _fh << layerID << ","; // Not sure if this is right...

      // layer_id
      _fh << sideID << ","; // Not sure if this is right...

      // volume_id
      _fh << ladderID << ","; // Not sure if this is right...

      // volume_id
      _fh << moduleID << ","; // Not sure if this is right...
      
      //
      // Hit position

      // tx,ty,tz
      _fh << hit->getPosition()[0] << "," << hit->getPosition()[1] << "," << hit->getPosition()[2] << ",";

      // tt
      _fh << hit->getTime() << ",";

      //
      // particle four-momentum before interaction

      // tpx,tpy,tpz
      _fh << hit->getMomentum()[0] << "," << hit->getMomentum()[1] << "," << hit->getMomentum()[2] << ",";

      // te
      _fh << hit->getMomentum()[3] << ",";

      //
      // particle four-momentum change due to interaction

      // deltapx,deltapy,deltapz
      _fh << 0 << "," << 0 << "," << 0 << ","; // TODO Implement

      // deltae
      _fh << hit->getdEdx()*hit->getPathLength() << ",";

      //
      // TODO write hit index along the particle trajectory
      _fh << i;
      
      // Done..
      _fh << std::endl;
    //_stori[i] = hit->ext<CollID>();

      /*
    _stci0[i] = hit->getCellID0() ;
    _stci1[i] = hit->getCellID1() ;
    _stpox[i] = hit->getPosition()[0] ;
    _stpoy[i] = hit->getPosition()[1] ;
    _stpoz[i] = hit->getPosition()[2] ;
    _stedp[i] = hit->getEDep() ;
    _sttim[i] = hit->getTime() ;
    _stmox[i] = hit->getMomentum()[0] ;
    _stmoy[i] = hit->getMomentum()[1] ;
    _stmoz[i] = hit->getMomentum()[2] ;
    _stptl[i] = hit->getPathLength() ;*/
    //_stmcp[i] = ( hit->getMCParticle() ? hit->getMCParticle()->ext<CollIndex>() -1 : -1 )  ;
    
    }
}






















